# Tool Setup

Install [Android Studio](https://developer.android.com/studio/)

Open Android Studio

Go to File menu and click *Settings*

In the Settings dialog, click *Plugins*

Click Browse repositories

In the Browse Repositories dialog that opens, click Manage repositories

Use the Custom Plugin Repositories dialog that opens, to manage the list of URLs for custom (enterprise) plugin repositories and add the rpository URL Repository URL : http://repo.devknox.io

Restart Android Studio

# Reproduce expreiment (manually)

Download the apks for all 42 benchmarks from [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) and save the apks in ghera-apks

For each benchmark open *Secure* in AndroidStudio. Click on the DevKnox Plugin on the menubar to run DevKnox. Compare the result with *results.txt* which lists each benchmark and an indicator that denotes if the vulnerability was detected or not. *D* means detected and *ND* means not detected.  
