export GHERA_HOME=~/ghera
export GHERA_APKS=~/rekha-eval/ghera-apks
export OUTPUT=~/rekha-eval/letterbomb/output
export LD_WKSPACE=~/rekha-eval/letterbomb/workspace/provisoriam

clean_data () {
  :
}

execute_wv () {
  cd $LD_WKSPACE
  outdir=`echo $1 | grep -o '[^/]*$' | cut -d'.' -f1`
  start=`date +%s`
  timeout 15m ./wv.sh $1
  end=`date +%s`
  runtime=$((end-start))
  mkdir $OUTPUT/xas/$outdir
  echo "$runtime s" > $OUTPUT/xas/$outdir/time.txt
  cp logs/edu.uci.seal.cases.analyses.WebViewTransformer-$outdir.apk-main.log $OUTPUT/xas/$outdir/
  cp $outdir.apk_xas_ic_tgt_units.txt $OUTPUT/xas/$outdir/
}

execute_nic () {
  cd $LD_WKSPACE
  outdir=`echo $1 | grep -o '[^/]*$' | cut -d'.' -f1`
  start=`date +%s`
  timeout 15m ./nic.sh $1
  end=`date +%s`
  runtime=$((end-start))
  mkdir $OUTPUT/nipc/$outdir
  echo "$runtime s" > $OUTPUT/nipc/$outdir/time.txt
  cp logs/edu.uci.seal.flow.reflect.nullipccheck.Driver-$outdir.apk-main.log $OUTPUT/nipc/$outdir/
  cp $outdir.apk_nic_ic_tgt_units.txt $OUTPUT/nipc/$outdir/
}

# echo "running LetterBomb on Benign apks"
# for b in `ls -1 $GHERA_APKS/benign/*.apk` ; do
#   echo "Processing $b"
#   execute_wv $b
#   execute_nic $b
#   echo "done with $b"
# done
echo "running Letterbomb on Secure apks"
for s in `ls -1 $GHERA_APKS/secure/*.apk` ; do
  echo "Processing $s"
  execute_wv $s
  execute_nic $s
  echo "done with $s"
done
echo "done"
