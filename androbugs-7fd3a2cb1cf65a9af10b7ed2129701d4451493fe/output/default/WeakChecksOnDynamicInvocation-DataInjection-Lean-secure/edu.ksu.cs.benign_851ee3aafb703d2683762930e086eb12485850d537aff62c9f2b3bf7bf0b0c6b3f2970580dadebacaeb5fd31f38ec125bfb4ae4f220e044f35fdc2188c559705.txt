*************************************************************************
**   AndroBugs Framework - Android App Security Vulnerability Scanner  **
**                            version: 1.0.0                           **
**     author: Yu-Cheng Lin (@AndroBugs, http://www.AndroBugs.com)     **
**               contact: androbugs.framework@gmail.com                **
*************************************************************************
Platform: Android
Package Name: edu.ksu.cs.benign
Package Version Name: 1.0
Package Version Code: 1
Min Sdk: 19
Target Sdk: 25
MD5   : 06276ae60c182648b0979870dcdd16a5
SHA1  : fb1ca47a5b4abe19c54a90c0a04154657e24c590
SHA256: 565531901f1d65d669a815ce6577ebc2b1bb995b6be6ba8138c14e5424434075
SHA512: 7be8467cf62022eb3012f7541eafd8c1609f144594e3e9c9a70c1e4ae5b0f9912755163ef0ed220cadb3d09a496af6f06fdbf1b60920ea0aa003666f129684d3
Analyze Signature: 851ee3aafb703d2683762930e086eb12485850d537aff62c9f2b3bf7bf0b0c6b3f2970580dadebacaeb5fd31f38ec125bfb4ae4f220e044f35fdc2188c559705
------------------------------------------------------------------------------------------------
[Critical] <Debug> Android Debug Mode Checking:
           DEBUG mode is ON(android:debuggable="true") in AndroidManifest.xml. This is very dangerous. The attackers will be able to sniffer
           the debug messages by Logcat. Please disable the DEBUG mode if it is a released application.
[Critical]  AndroidManifest Dangerous ProtectionLevel of Permission Checking:
           The protection level of the below classes is "dangerous", allowing any other apps to access this permission
           (AndroidManifest.xml).
           The app should declare the permission with the "android:protectionLevel" of "signature" or "signatureOrSystem" so that other apps
           cannot register and receive message for this app.
           android:protectionLevel="signature" ensures that apps with request a permission must be signed with same certificate as the
           application that declared the permission.
           Please check some related cases: http://www.wooyun.org/bugs/wooyun-2010-039697
           Please change these permissions:
               edu.ksu.cs.benign.filecontentprovider.wperm
                   -> used by (provider) edu.ksu.cs.benign.FIleContentProvider
               edu.ksu.cs.benign.filecontentprovider.rperm
                   -> used by (provider) edu.ksu.cs.benign.FIleContentProvider
[Critical]  AndroidManifest ContentProvider Exported Checking:
           Found "exported" ContentProvider, allowing any other app on the device to access it (AndroidManifest.xml). You should modify the
           attribute to [exported="false"] or set at least "signature" protectionalLevel permission if you don't want to.
           Vulnerable ContentProvider Case Example:
             (1)https://www.nowsecure.com/mobile-security/ebay-android-content-provider-injection-vulnerability.html
             (2)http://blog.trustlook.com/2013/10/23/ebay-android-content-provider-information-disclosure-vulnerability/
             (3)http://www.wooyun.org/bugs/wooyun-2010-039169
                 provider => edu.ksu.cs.benign.FIleContentProvider
[Info]  AndroidManifest Adb Backup Checking:
           This app has disabled Adb Backup.
[Info] <Command> Runtime Command Checking:
           This app is not using critical function 'Runtime.getRuntime().exec("...")'.
[Info] <Command> Executing "root" or System Privilege Checking:
           Did not find codes checking "root" permission(su) or getting system permission (It's still possible we did not find out).
[Info] <Database> SQLiteDatabase Transaction Deprecated Checking:
           Ignore checking "SQLiteDatabase:beginTransactionNonExclusive" because your set minSdk >= 11.
[Info] <Database> Android SQLite Databases Encryption (SQLite Encryption Extension (SEE)):
           This app is "NOT" using SQLite Encryption Extension (SEE) on Android (http://www.sqlite.org/android) to encrypt or decrpyt
           databases.
[Info] <Database> Android SQLite Databases Encryption (SQLCipher):
           This app is "NOT" using SQLCipher(http://sqlcipher.net/) to encrypt or decrpyt databases.
[Info] <Database><#CVE-2011-3901#> Android SQLite Databases Vulnerability Checking:
           This app is "NOT" using Android SQLite databases.
[Info]  Dynamic Code Loading:
           No dynamic code loading(DexClassLoader) found.
[Info]  External Storage Accessing:
           External storage access not found.
[Info]  File Unsafe Delete Checking:
           Did not detect that you are unsafely deleting files.
[Info] <#BID 64208, CVE-2013-6271#> Fragment Vulnerability Checking:
           Did not detect the vulnerability of "Fragment" dynamically loading into "PreferenceActivity" or "SherlockPreferenceActivity"
[Info] <Framework> Framework - MonoDroid:
           This app is NOT using MonoDroid Framework (http://xamarin.com/android).
[Info] <Hacker> Base64 String Encryption:
           No encoded Base64 String or Urls found.
[Info] <Database><Hacker> Key for Android SQLite Databases Encryption:
           Did not find using the symmetric key(PRAGMA key) to encrypt the SQLite databases (It's still possible that it might use but we
           did not find out).
[Info] <Debug><Hacker> Codes for Checking Android Debug Mode:
           Did not detect codes for checking "ApplicationInfo.FLAG_DEBUGGABLE" in AndroidManifest.xml.
[Info] <Hacker> APK Installing Source Checking:
           Did not detect this app checks for APK installer sources.
[Info] <KeyStore><Hacker> KeyStore File Location:
           Did not find any possible BKS keystores or certificate keystore file (Notice: It does not mean this app does not use keysotre):
[Info] <KeyStore><Hacker> KeyStore Protection Checking:
           Ignore checking KeyStore protected by password or not because you're not using KeyStore.
[Info] <Hacker> Code Setting Preventing Screenshot Capturing:
           Did not detect this app has code setting preventing screenshot capturing.
[Info] <Signature><Hacker> Getting Signature Code Checking:
           Did not detect this app is checking the signature in the code.
[Info]  HttpURLConnection Android Bug Checking:
           Ignore checking "http.keepAlive" because you're not using "HttpURLConnection" and min_Sdk > 8.
[Info] <KeyStore> KeyStore Type Checking:
           KeyStore 'BKS' type check OK
[Info]  Google Cloud Messaging Suggestion:
           Nothing to suggest.
[Info] <#CVE-2013-4787#> Master Key Type I Vulnerability:
           No Master Key Type I Vulnerability in this APK.
[Info]  App Sandbox Permission Checking:
           No security issues "MODE_WORLD_READABLE" or "MODE_WORLD_WRITEABLE" found on 'openOrCreateDatabase' or 'openOrCreateDatabase2' or
           'getDir' or 'getSharedPreferences' or 'openFileOutput'
[Info]  Native Library Loading Checking:
           No native library loaded.
[Info]  AndroidManifest Exported Components Checking:
           No exported components(except for Launcher) for receiving Android or outside applications' actions (AndroidManifest.xml).
[Info]  AndroidManifest PermissionGroup Checking:
           PermissionGroup in permission tag of AndroidManifest sets correctly.
[Info] <Implicit_Intent> Implicit Service Checking:
           No dangerous implicit service.
[Info]  AndroidManifest "intent-filter" Settings Checking:
           "intent-filter" of AndroidManifest.xml check OK.
[Info]  AndroidManifest Normal ProtectionLevel of Permission Checking:
           No default or "normal" protection level customized permission found (AndroidManifest.xml).
[Info] <#CVE-2013-6272#> AndroidManifest Exported Lost Prefix Checking:
           No exported components that forgot to add "android:" prefix.
[Info] <Sensitive_Information> Getting IMEI and Device ID:
           Did not detect this app is getting the "device id(IMEI)" by "TelephonyManager.getDeviceId()" approach.
[Info] <Sensitive_Information> Getting ANDROID_ID:
           Did not detect this app is getting the 64-bit number "Settings.Secure.ANDROID_ID".
[Info]  Codes for Sending SMS:
           Did not detect this app has code for sending SMS messages (sendDataMessage, sendMultipartTextMessage or sendTextMessage).
[Info] <System> AndroidManifest sharedUserId Checking:
           This app does not use "android.uid.system" sharedUserId.
[Info] <SSL_Security> SSL Implementation Checking (Verifying Host Name in Custom Classes):
           Self-defined HOSTNAME VERIFIER checking OK.
[Info] <SSL_Security> SSL Implementation Checking (Verifying Host Name in Fields):
           Critical vulnerability "ALLOW_ALL_HOSTNAME_VERIFIER" field setting or "AllowAllHostnameVerifier" class instance not found.
[Info] <SSL_Security> SSL Implementation Checking (Insecure component):
           Did not detect SSLSocketFactory by insecure method "getInsecure".
[Info] <SSL_Security> SSL Implementation Checking (HttpHost):
           DEFAULT_SCHEME_NAME for HttpHost check: OK
[Info] <SSL_Security> SSL Connection Checking:
           Did not discover urls that are not under SSL (Notice: if you encrypt the url string, we can not discover that).
[Info] <SSL_Security> SSL Implementation Checking (WebViewClient for WebView):
           Did not detect critical usage of "WebViewClient"(MITM Vulnerability).
[Info] <SSL_Security> SSL Certificate Verification Checking:
           Did not find vulnerable X509Certificate code.
[Info]  Unnecessary Permission Checking:
           Permission 'android.permission.ACCESS_MOCK_LOCATION' sets correctly.
[Info]  Accessing the Internet Checking:
           No HTTP-related connection codes found.
[Info]  AndroidManifest System Use Permission Checking:
           No system-level critical use-permission found.
[Info] <WebView> WebView Local File Access Attacks Checking:
           Did not find potentially critical local file access settings.
[Info] <WebView> WebView Potential XSS Attacks Checking:
           Did not detect "setJavaScriptEnabled(true)" in WebView.
[Info] <WebView><Remote Code Execution><#CVE-2013-4710#> WebView RCE Vulnerability Checking:
           WebView addJavascriptInterface vulnerabilities not found.
------------------------------------------------------------
AndroBugs analyzing time: 1.654502 secs
Total elapsed time: 7.965966 secs
