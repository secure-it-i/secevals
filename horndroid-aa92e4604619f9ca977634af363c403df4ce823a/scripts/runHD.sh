export LD_LIBRARY_PATH=/home/joydeep/z3/build
GHERA_HOME=~/ghera
GHERA_APKS=~/rekha-eval/ghera-apks
OUTPUT=~/rekha-eval/horndroid/output
TARGET=~/rekha-eval/horndroid/target

clean_data () {
  rm $1/app.log
}

execute () {
  cd $TARGET
  outdir=`echo $1 | grep -o '[^/]*$' | cut -d'.' -f1`
  start=`date +%s`
  timeout 15m java -jar fshorndroid-0.0.1.jar / ./ $1
  end=`date +%s`
  runtime=$((end-start))
  mkdir $OUTPUT/default/$outdir/
  echo "$runtime s" > $OUTPUT/default/$outdir/time.txt
  cp $TARGET/logs/app.log $OUTPUT/default/$outdir/
  clean_data $TARGET/logs
  cd ..
}

# echo "running HornDroid on Benign apks"
# for b in `ls -1 $GHERA_APKS/benign/*.apk` ; do
#   echo "Processing $b"
#   execute $b
#   echo "done with $b"
# done
echo "running Amandroid on Secure apks"
for s in `ls -1 $GHERA_APKS/secure/*.apk` ; do
  echo "Processing $s"
  execute $s
  echo "done with $s"
done
echo "running Amandroid on Secure apks"
for m in `ls -1 $GHERA_APKS/malicious/*.apk` ; do
  echo "Processing $m"
  execute $m
  echo "done with $m"
done
echo "done"
