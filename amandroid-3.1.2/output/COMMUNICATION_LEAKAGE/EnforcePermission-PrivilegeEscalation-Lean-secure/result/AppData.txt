Application Name: EnforcePermission-PrivilegeEscalation-Lean-secure.apk
Uses Permissions: santos.benign.permission

Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startService:(Landroid/content/Intent;)Landroid/content/ComponentName;
      Caller Context: (MainActivity.onRequestPermissionsResult,L1673be)(MainActivity.envMain,L15)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.MyService
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.MyService
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startService:(Landroid/content/Intent;)Landroid/content/ComponentName;
      Caller Context: (MainActivity.onResume,L167444)(MainActivity.envMain,L10)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.MyService
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.MyService

Component edu.ksu.cs.benign.MyService
  Component type: service
  Exported: true
  Dynamic Registered: false
  Required Permission: santos.benign.permission
  IntentFilters:

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (MyService.onStartCommand,L10d1ba)(MyService.envMain,L33)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.SensitiveActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.SensitiveActivity

Component edu.ksu.cs.benign.SensitiveActivity
  Component type: activity
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startService:(Landroid/content/Intent;)Landroid/content/ComponentName;
      Caller Context: (MainActivity.onRequestPermissionsResult,L1673be)(SensitiveActivity.env,L58)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.MyService
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.MyService


Taint analysis result:
  Sources found:
  Sinks found:
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
  Discovered taint paths are listed below:
