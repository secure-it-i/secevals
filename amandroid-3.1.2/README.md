# Tool Setup

Download the jar file

# Reproduce experiment(automatically)

`$ mkdir ghera-apks`

Download the apks for all 42 benchmarks from [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) and save the apks in ghera-apks

`$ cd scripts`

`$ ./runAmandroid.sh`

# Results

See the evaluation results in *output/<config>/<benchmark>/result*

If the tool failed to process the benchmark then the error will be captured in *output/<config>/<benchmark>/.errorlog*

# Reproduce experiment (manually)

`$ java -jar argus-saf_2.12-3.1.2-assembly.jar t -mo <module-name> -o <output> <apk>`

module-name: Taint analysis module to use. [Default: DATA_LEAKAGE, Choices: (COMMUNICATION_LEAKAGE, OAUTH_TOKEN_TRACKING, PASSWORD_TRACKING, INTENT_INJECTION, DATA_LEAKAGE)]

`java -jar argus-saf_2.12-3.1.2-assembly.jar a -c <check> -o <output> <apk>`

check: CRYPTO_MISUSE or SSLTLS_MISUSE

# References

[Argus-SAF](http://pag.arguslab.org/argus-saf)
