This repository captures results of evaluating freely available tools that detect vulnerabilities in Android apps.

The following tools were evaluated:

1. **Amandroid**
A static analysis framework to do security vetting for Android applications

2. **AndroBugs**
An Android vulnerability analysis system that helps developers or hackers find potential security vulnerabilities in Android applications

3. **AppCritique**
Automated analysis to detect vulnerabilities in Android apps

4. **Covert**
Compositional verification of Android inter-application vulnerabilities. It automatically identifies vulnerabilities that occur due to the interaction of apps comprising a system.

5. **DevKnox**
An Android Studio plugin that warns developers of potential vulnerabilities in their apps

6. **DIALdroid**
A highly scalable tool to identify inter-app collusions and privilege escalations among Android apps.

7. **FlowDroid**
A context-, flow-, field-, object-sensitive and lifecycle-aware static taint analysis tool for Android applications

8. **HornDroid**
A static analysis tool for Android apps to detect privacy leaks

9. **JAADS**
A static analysis tool to do static taint analysis, API misuse, and inter- and intra-procedural analysis of Android apps

10. **LetterBomb**
A tool to automatically generate exploits for Android apps

11. **Mallodroid**
A tool to automatically detect vulnerabilities related to SSL misuse

12. **Marvin**
A static analysis tool to detect vulnerabilities in Android apps

13. **MobSF**
An automated, all-in-one mobile application (Android/iOS/Windows) pen-testing framework capable of performing static, dynamic and malware analysis

14. **Qark**
A tool to look for several security related Android application vulnerabilities, either in source code or packaged APKs


Each tool was run against *Secure* app of each benchmark in [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) - an open-source repository of Android app vulnerability benchmarks. A tool is expected to not detect the vulnerability in the secure app. This repository contains the results of runnning the tools against the Secure apps in each benchmark. The results of the corresponding *Benign* apps are available in [vulevals](https://bitbucket.org/secure-it-i/vulevals/src). The results of each tool run is captured in a folder named after tool-version (if version info for the tool was available). The version is the release version of the tool. If the release version is not available then the commit id of the repository used for evaluation is the version. Each folder also contains a compressed form of the tool that was used for evaluation. The *output* folder for each tool folder contains subfolders indicating the configuration in which the tool was evaluated. Each configuration folder contains the reports of the evaluation for each benchmark (Secure app).

# Contact

Please raise an issue if you notice discrepancies or have questions about the results.
