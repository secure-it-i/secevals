export GHERA_HOME=~/ghera
export GHERA_APKS=~/rekha-eval/ghera-apks
export OUTPUT=~/rekha-eval/DIALDroid/output/default
export DIALDROID_HOME=~/rekha-eval/DIALDroid

clean_data () {
  rm -rf $DIALDROID_HOME/ghera_repo
  echo "clean up done!"
}

execute () {
  cd $DIALDROID_HOME/build
  apkname=`echo $1 | grep -o '[^/]*$' | cut -d'.' -f1`
  if [ ! -d $DIALDROID_HOME/ghera_repo ] ; then
    mkdir $DIALDROID_HOME/ghera_repo
  fi
  if [ $2 == "benign" ] ; then
    if [ ! -d $OUTPUT/benign ] ; then
      mkdir $OUTPUT/benign
    fi
  fi
  if [ $2 == "secure" ] ; then
    if [ ! -d $OUTPUT/secure ] ; then
      mkdir $OUTPUT/secure
    fi
  fi
  if [ ! -d $OUTPUT/$2/$apkname ] ; then
    mkdir $OUTPUT/$2/$apkname
  fi
  cp $1 $DIALDROID_HOME/ghera_repo
  start=`date +%s`
  ./dialdroid.sh $DIALDROID_HOME/ghera_repo/ debug > $OUTPUT/$2/$apkname/log.txt 2> $OUTPUT/$2/$apkname/err.txt
  end=`date +%s`
  runtime=$((end-start))
  echo "$runtime s" > $OUTPUT/$2/$apkname/time.txt
  cd $DIALDROID_HOME
}

# echo "running DialDroid on Benign apks"
# for b in `ls -1 $GHERA_APKS/benign/*.apk` ; do
#   echo "Processing $b"
#   execute $b "benign"
#   clean_data
#   echo "done with $b"
# done
echo "running DialDroid on Secure apks"
for s in `ls -1 $GHERA_APKS/secure/*.apk` ; do
  echo "Processing $s"
  execute $s "secure"
  clean_data
  echo "done with $s"
done
echo "done"
