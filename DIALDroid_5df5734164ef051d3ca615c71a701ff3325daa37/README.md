# Tool Setup

Download the tar file

# Reproduce experiment(automatically)

`$ mkdir ghera-apks`

Download the apks for all 42 benchmarks from [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) and save the apks in ghera-apks

`$ cd scripts`

`$ ./runDIALDROID.sh`

# Results

See the evaluation results in *output/<config>/<benchmark>/log.txt*

If the tool failed to process the benchmark then the error will be captured in *output/<config>/<benchmark>/err.txt*

# Reproduce experiment (manually)

Copy *Benign* and *Secure* of each benchmark in *DIALDroid/ghera_repo*

`$ DIALDroid/build/dialdroid.sh DIALDroid/ghera_repo debug > <output>/log.txt 2> <output>/err.txt`

# References

[DIALDroid](https://github.com/dialdroid-android/DIALDroid)
