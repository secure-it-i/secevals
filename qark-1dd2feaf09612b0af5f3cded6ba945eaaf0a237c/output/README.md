Qark can be run on an apk file or directly on the source code of an app. Qark was run against

1. *Benign* and *Secure* apk files in Ghera (see apk folder) and
2. the source code of *Benign* and *Secure* in Ghera (see src folder)

Please note that any warnings related to the *debug status of the app*  have been ignored in our analysis of the tool results e.g. *android:debuggable=true*  
