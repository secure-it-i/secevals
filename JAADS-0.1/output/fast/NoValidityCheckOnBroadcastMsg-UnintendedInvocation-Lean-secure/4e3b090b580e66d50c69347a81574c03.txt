{
  "score": 25.0,
  "md5hash": "4e3b090b580e66d50c69347a81574c03",
  "results": [{
    "desc": "application is debuggable",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 3,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": ""
  }, {
    "desc": "implicit intent or receiver",
    "sourceStmt": "virtualinvoke $r0.<edu.ksu.cs.benign.DeleteFilesIntentService: void startActivity(android.content.Intent)>($r1)",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.DeleteFilesIntentService: void onHandleIntent(android.content.Intent)>"
  }, {
    "desc": "implicit intent or receiver",
    "sourceStmt": "virtualinvoke $r0.<edu.ksu.cs.benign.DeleteFilesIntentService: void startActivity(android.content.Intent)>($r1)",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.DeleteFilesIntentService: void onHandleIntent(android.content.Intent)>"
  }]
}